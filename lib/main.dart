import 'package:flutter/material.dart';
import 'package:test_project/presentation/auth_pages/reset_password.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Project',
      theme: ThemeData(
        primaryColor: const Color(0xff3683fc),
        fontFamily: 'DMSans',
      ),
      home: const ResetPasswordPage(),
    );
  }
}
