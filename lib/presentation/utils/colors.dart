import 'package:flutter/cupertino.dart';

const kMainColor = Color(0xff3683fc);
const kDisabledColor = Color(0xff8E8F9C);

const kGrayColor95 = Color(0xff959595);
const kGrayColorE1 = Color(0xffE1E1E1);
