import 'package:flutter/material.dart';

class EmailAddress extends ValueNotifier<String> {
  EmailAddress() : super('');
}

class VerificationCode extends ValueNotifier<String> {
  VerificationCode() : super('');
}

class PasswordProvider extends ValueNotifier<Password> {
  PasswordProvider() : super(Password('', false));
}

class Password {
  String password;
  bool isVisible;

  Password(this.password, this.isVisible);
}
