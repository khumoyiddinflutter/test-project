import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'auth_item_presentation_classes.dart';

extension EmailX on BuildContext {
  //Email address Provider
  String get emailTextField =>
      Provider.of<EmailAddress>(this, listen: false).value;

  bool get validateEmailAddress {
    const emailRegex =
        r"""^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+""";
    if (RegExp(emailRegex)
        .hasMatch(Provider.of<EmailAddress>(this, listen: false).value)) {
      return true;
    } else {
      return false;
    }
  }

  set emailTextField(value) =>
      Provider.of<EmailAddress>(this, listen: false).value = value;

  //Verification code Provider
  String get verificationCodeText =>
      Provider.of<VerificationCode>(this, listen: false).value;

  bool get validateVerificationCode {
    if (Provider.of<VerificationCode>(this, listen: false).value.length == 6) {
      return true;
    } else {
      return false;
    }
  }

  set verificationCodeText(value) =>
      Provider.of<VerificationCode>(this, listen: false).value = value;

  //Password Provider
  Password get passwordText =>
      Provider.of<PasswordProvider>(this, listen: false).value;

  bool get validatePassword {
    final passwordLength = Provider.of<PasswordProvider>(this, listen: false)
        .value
        .password
        .length;
    if (passwordLength >= 6 && passwordLength <= 10) {
      return true;
    } else {
      return false;
    }
  }

  set passwordText(value) =>
      Provider.of<PasswordProvider>(this, listen: false).value = value;

}
