import 'package:flutter/material.dart';
import 'package:test_project/presentation/utils/colors.dart';

class CustomOutlinedButton extends StatelessWidget {
  const CustomOutlinedButton({
    Key? key,
    required this.title,
    required this.isActive,
    this.onTap,
  }) : super(key: key);
  final String title;
  final bool isActive;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: isActive ? onTap : null,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 9,
          horizontal: 59,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: isActive ? kMainColor : kDisabledColor,
          ),
        ),
      ),
      style: OutlinedButton.styleFrom(
        side: BorderSide(
          color: isActive ? kMainColor : kDisabledColor,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    );
  }
}
