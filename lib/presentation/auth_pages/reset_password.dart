import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:provider/provider.dart';
import 'package:test_project/presentation/auth_pages/misc/auth_item_presentation_classes.dart';
import 'package:test_project/presentation/auth_pages/misc/build_context_x.dart';
import 'package:test_project/presentation/auth_pages/verification_page.dart';
import 'package:test_project/presentation/auth_pages/widgets/custom_buttom.dart';

import '../utils/colors.dart';
import 'widgets/custom_text_form_field.dart';

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => EmailAddress(),
      child: KeyboardDismisser(
        child: Scaffold(
          body: _ResetPasswordBody(),
        ),
      ),
    );
  }
}

class _ResetPasswordBody extends StatelessWidget {
  _ResetPasswordBody({
    Key? key,
  }) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: _screenSize.height * 0.12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 129),
              child: Image.asset(
                'assets/icons/logo.png',
              ),
            ),
            SizedBox(
              height: _screenSize.height * 0.08,
            ),
            const Text(
              "Reset Password",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 6),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: Text(
                "If you've forgotten your password enter your e-mail address and we send you a verification code, then you can reset your password.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: kGrayColor95,
                ),
              ),
            ),
            SizedBox(height: _screenSize.height * 0.14),
            CustomTextFormField(
              hintText: 'Enter your Email ID',
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                } else if (!context.validateEmailAddress) {
                  return 'Please enter valid email address';
                }
                return null;
              },
              onChanged: (value) {
                context.emailTextField = value;
              },
              keyboardType: TextInputType.emailAddress,
            ),
            SizedBox(height: _screenSize.height * 0.22),
            Consumer<EmailAddress>(
              builder: (context, value, child) {
                return CustomOutlinedButton(
                  title: 'Confirm',
                  isActive: value.value.isNotEmpty,
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const VerificationPage(),
                        ),
                      );
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
