import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:provider/provider.dart';
import 'package:test_project/presentation/auth_pages/misc/auth_item_presentation_classes.dart';
import 'package:test_project/presentation/auth_pages/misc/build_context_x.dart';
import 'package:test_project/presentation/auth_pages/new_password_page.dart';
import 'package:test_project/presentation/auth_pages/widgets/custom_buttom.dart';
import 'package:test_project/presentation/auth_pages/widgets/custom_text_form_field.dart';

import '../utils/colors.dart';

class VerificationPage extends StatelessWidget {
  const VerificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => VerificationCode(),
      child: KeyboardDismisser(
        child: Scaffold(
          body: _VerificationBody(),
        ),
      ),
    );
  }
}

class _VerificationBody extends StatelessWidget {
  _VerificationBody({
    Key? key,
  }) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: _screenSize.height * 0.12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 129),
              child: Image.asset(
                'assets/icons/logo.png',
              ),
            ),
            SizedBox(
              height: _screenSize.height * 0.08,
            ),
            const Text(
              "Enter Code",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 6),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: Text(
                "Enter your six digit verification code we have sent you in your email.\n\n",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: kGrayColor95,
                ),
              ),
            ),
            SizedBox(height: _screenSize.height * 0.14),
            CustomTextFormField(
              hintText: 'Enter Verification Code',
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter verification code';
                } else if (!context.validateVerificationCode) {
                  return 'Must be 6 digits';
                }
                return null;
              },
              onChanged: (value) {
                context.verificationCodeText = value;
              },
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            ),
            SizedBox(height: _screenSize.height * 0.22),
            Consumer<VerificationCode>(
              builder: (context, value, child) {
                return CustomOutlinedButton(
                  title: 'Confirm',
                  isActive: value.value.isNotEmpty,
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const NewPasswordPage(),
                        ),
                      );
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
