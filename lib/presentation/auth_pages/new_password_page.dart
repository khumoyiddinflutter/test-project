import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:provider/provider.dart';
import 'package:test_project/presentation/auth_pages/misc/auth_item_presentation_classes.dart';
import 'package:test_project/presentation/auth_pages/misc/build_context_x.dart';
import 'package:test_project/presentation/auth_pages/widgets/custom_buttom.dart';

import '../utils/colors.dart';

class NewPasswordPage extends StatelessWidget {
  const NewPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => PasswordProvider(),
      child: KeyboardDismisser(
        child: Scaffold(
          body: _NewPasswordBody(),
        ),
      ),
    );
  }
}

class _NewPasswordBody extends StatelessWidget {
  _NewPasswordBody({
    Key? key,
  }) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: _screenSize.height * 0.12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 129),
              child: Image.asset(
                'assets/icons/logo.png',
              ),
            ),
            SizedBox(
              height: _screenSize.height * 0.08,
            ),
            const Text(
              "New Password",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 6),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: Text(
                "Enter your new passord, please, try to use numbers and english letters in it, you need to enter from 6 to 10 symbols.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: kGrayColor95,
                ),
              ),
            ),
            SizedBox(height: _screenSize.height * 0.14),
            Consumer<PasswordProvider>(
              builder: (_, value, __) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter new password';
                      }
                      if (!context.validatePassword) {
                        return ' Enter new password from 6 to 10 symbols';
                      }
                      return null;
                    },
                    onChanged: (text) {
                      context.passwordText =
                          Password(text, context.passwordText.isVisible);
                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    obscureText: !value.value.isVisible,
                    obscuringCharacter: '*',
                    decoration: InputDecoration(
                      hintText: 'Enter New Password',
                      suffixIcon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: GestureDetector(
                          onTap: () {
                            context.passwordText = Password(
                              context.passwordText.password,
                              !context.passwordText.isVisible,
                            );
                          },
                          child: SvgPicture.asset(
                            'assets/icons/visable.svg',
                            color: value.value.isVisible
                                ? kMainColor
                                : kDisabledColor,
                          ),
                        ),
                      ),
                      suffixIconConstraints:
                          const BoxConstraints(minWidth: 0, minHeight: 0),
                      hintStyle: const TextStyle(
                        color: kDisabledColor,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: kGrayColorE1,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: kMainColor,
                        ),
                      ),
                      isDense: true,
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: _screenSize.height * 0.22),
            Consumer<PasswordProvider>(
              builder: (context, value, child) {
                return CustomOutlinedButton(
                  title: 'Confirm',
                  isActive: value.value.password.isNotEmpty,
                  onTap: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
